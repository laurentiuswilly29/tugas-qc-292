package com.example.employeeLeave.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/employee/")
public class EmployeeController {

	@GetMapping(value = "index")
	public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("employee/index");
        return view;
    }
}
