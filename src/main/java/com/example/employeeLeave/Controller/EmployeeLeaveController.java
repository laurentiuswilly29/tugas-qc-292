package com.example.employeeLeave.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.example.employeeLeave.Repository.EmployeeRepo;

@Controller
@RequestMapping("/employeeleave/")
public class EmployeeLeaveController {


	@GetMapping(value = "index")
		public ModelAndView index()
	    {
	        ModelAndView view = new ModelAndView("employeeleave/index");
	        return view;
	    }
}
