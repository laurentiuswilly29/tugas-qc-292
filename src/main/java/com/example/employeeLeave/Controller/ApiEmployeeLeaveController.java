package com.example.employeeLeave.Controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.employeeLeave.Model.EmployeeLeave;
import com.example.employeeLeave.Repository.EmployeeLeaveRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")

public class ApiEmployeeLeaveController {

    @Autowired
    private EmployeeLeaveRepo employeeleaveRepo;

    @GetMapping("/employeeleave")
    public ResponseEntity<List<EmployeeLeave>> GetAllEmployeeLeave()
    {
        try {
            List<EmployeeLeave> employeeleave = this.employeeleaveRepo.findAll();
            return new ResponseEntity<>(employeeleave, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/employeeleave")
    public ResponseEntity<Object> SaveEmployeeLeave(@RequestBody EmployeeLeave employeeleave)
    {
        try {
            this.employeeleaveRepo.save(employeeleave);
            return new ResponseEntity<>(employeeleave, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/employeeleave/{id}")
    public ResponseEntity<List<EmployeeLeave>> GetEmployeeLeaveById(@PathVariable("id") Long id)
    {
        try {
            Optional<EmployeeLeave> employeeleave = this.employeeleaveRepo.findById(id);

            if (employeeleave.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(employeeleave, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchemployeeleave/{keyword}")
    public ResponseEntity<List<EmployeeLeave>> SearchEmployeeLeaveName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<EmployeeLeave> employeeleave = this.employeeleaveRepo.SearchEmployeeLeave(keyword);
            return new ResponseEntity<>(employeeleave, HttpStatus.OK);
        } else {
            List<EmployeeLeave> employeeleave = this.employeeleaveRepo.findAll();
            return new ResponseEntity<>(employeeleave, HttpStatus.OK);
        }
    }

    @GetMapping("/employeeleavemapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<EmployeeLeave> employeeleave = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<EmployeeLeave> pageTuts;

            pageTuts = employeeleaveRepo.findAll(pagingSort);

            employeeleave = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("employeeleave", employeeleave);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/employeeleave/{id}")
    public ResponseEntity<Object> UpdateEmployeeleave(@RequestBody EmployeeLeave employeeleave, @PathVariable("id") Long id)
    {
        Optional<EmployeeLeave> employeeleaveData = this.employeeleaveRepo.findById(id);

        if (employeeleaveData.isPresent())
        {
            employeeleave.setId(id);
            this.employeeleaveRepo.save(employeeleave);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/employeeleave/{id}")
    public ResponseEntity<Object> EmployeeleaveDelete(@PathVariable("id") Long id)
    {
        this.employeeleaveRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}
