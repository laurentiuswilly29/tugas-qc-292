package com.example.employeeLeave.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.employeeLeave.Model.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    @Query("FROM Employee WHERE lower(Nip) LIKE lower(concat('%',?1,'%') ) ")
    List<Employee> SearchEmployee(String keyword);
    @Query("FROM Employee WHERE BiodataId = ?1")
    List<Employee> FindByBiodataId(Long variantId);
}
