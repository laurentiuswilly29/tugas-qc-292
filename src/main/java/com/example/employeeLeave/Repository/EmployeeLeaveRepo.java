package com.example.employeeLeave.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.employeeLeave.Model.EmployeeLeave;

public interface EmployeeLeaveRepo extends JpaRepository<EmployeeLeave, Long> {

	@Query("FROM EmployeeLeave WHERE lower(Period) LIKE lower(concat('%',?1,'%') ) ")
    List<EmployeeLeave> SearchEmployeeLeave(String keyword);
}
