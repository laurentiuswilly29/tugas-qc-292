package com.example.employeeLeave.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.employeeLeave.Model.BiodataEmployee;


public interface BiodataEmployeeRepo extends JpaRepository<BiodataEmployee, Long>{
    @Query("FROM BiodataEmployee WHERE lower(FirstName) LIKE lower(concat('%',?1,'%') ) ")
    List<BiodataEmployee> SearchBiodata(String keyword);
}
