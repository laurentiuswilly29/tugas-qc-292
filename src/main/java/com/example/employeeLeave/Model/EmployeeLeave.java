package com.example.employeeLeave.Model;

import javax.persistence.*;

@Entity
@Table(name = "employee_leave")
public class EmployeeLeave {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long Id;
	
	@Column(name = "employee_id", nullable = false)
	private long EmployeeId;
	
	@Column(name = "period", nullable = false)
	private String Period;
	
	@Column(name = "regular_quota", nullable = false)
	private int RegularQuota;
	
	@ManyToOne
	@JoinColumn(name = "employee_id", insertable = false, updatable = false)
	public Employee employee;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public long getEmployeeId() {
		return EmployeeId;
	}

	public void setEmployeeId(long employeeId) {
		EmployeeId = employeeId;
	}

	public String getPeriod() {
		return Period;
	}

	public void setPeriod(String period) {
		Period = period;
	}

	public int getRegularQuota() {
		return RegularQuota;
	}

	public void setRegularQuota(int regularQuota) {
		RegularQuota = regularQuota;
	}
	
	
}
