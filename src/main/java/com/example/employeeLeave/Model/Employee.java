package com.example.employeeLeave.Model;

import javax.persistence.*;

@Entity
@Table(name = "employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long Id;
	
	@Column(name = "biodata_id", nullable = false)
	private long BiodataId;
	
	@Column(name = "nip", nullable = false)
	private String Nip;
	
	@Column(name = "status", nullable = false)
	private String Status;
	
	@Column(name = "salary", nullable = false)
	private int Salary;
	
	@OneToOne
	@JoinColumn(name = "biodata_id", insertable = false, updatable = false)
	public BiodataEmployee biodata;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public long getBiodataId() {
		return BiodataId;
	}

	public void setBiodataId(long biodataId) {
		BiodataId = biodataId;
	}

	public String getNip() {
		return Nip;
	}

	public void setNip(String nip) {
		Nip = nip;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getSalary() {
		return Salary;
	}

	public void setSalary(int salary) {
		Salary = salary;
	}
	
	
}
